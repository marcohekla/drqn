import numpy as np
import random
import torch

from helper import *

from gridworld import gameEnv

from models.Q import Q

env = gameEnv(partial=False, size=9)
env = gameEnv(partial=True, size=9)

# Setting the training parameters
batch_size = 4 #How many experience traces to use for each training step.
trace_length = 8 #How long each experience trace will be when training
update_freq = 5 #How often to perform a training step.
y = .99 #Discount factor on the target Q-values
startE = 1 #Starting chance of random action
endE = 0.1 #Final chance of random action
anneling_steps = 10000 #How many steps of training to reduce startE to endE.
num_episodes = 10000 #How many episodes of game environment to train network with.
pre_train_steps = 10000 #How many steps of random actions before training begins.
load_model = False #Whether to load a saved model.
path = "./drqn" #The path to save our model to.
h_size = 512 #The size of the final convolutional layer before splitting it into Advantage and Value streams.
max_epLength = 50 #The max allowed length of our episode.
time_per_step = 1 #Length of each step used in gif creation
summaryLength = 100 #Number of epidoes to periodically save for analysis
tau = 0.001

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# These classes allow us to store experies and sample then randomly to train the network. Episode buffer stores
# experiences for each individal episode. Experience buffer stores entire episodes of experience, and sample()
# allows us to get training batches needed from the network.

class experience_buffer():
    def __init__(self, buffer_size=1000):
        self.buffer = []
        self.buffer_size = buffer_size

    def add(self, experience):
        if len(self.buffer) + 1 >= self.buffer_size:
            # if len==999 then nothing is added as self.buffer[0:0] is not defined.
            # if len==1000 then buffer[0:1] = [] removed the first element.
            # weird way of popping stuff...
            self.buffer[0:(1 + len(self.buffer)) - self.buffer_size] = []
        self.buffer.append(experience)

    def sample(self, batch_size, trace_length):
        sampled_episodes = random.sample(self.buffer, batch_size)
        sampledTraces = []
        for episode in sampled_episodes:
            point = np.random.randint(0, len(episode) + 1 - trace_length)
            sampledTraces.append(episode[point:point + trace_length])
        sampledTraces = np.array(sampledTraces)
        return np.reshape(sampledTraces, [batch_size * trace_length, 5])


mainQN = Q(device=device, h_size=h_size).to(device)
targetQN = Q(device=device, h_size=h_size).to(device)

optimizer = torch.optim.Adam(mainQN.parameters(), lr=0.0001)

myBuffer = experience_buffer()

# Set the rate of random action decrease
e = startE
stepDrop = (startE - endE)/anneling_steps

# create lists to contain total rewards and steps per episode
jList = []
rList = []
total_steps = 0

# Make a path for our model to be saved in.
if not os.path.exists(path):
    os.makedirs(path)

# Write the first line of the master log-file for the Control Center
with open('./Center/log.csv', 'w') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(['Episode', 'Length', 'Reward', 'IMG', 'LOG', 'SAL'])

# update target network
targetQN.load_state_dict(mainQN.state_dict())

targetQN.eval()

for i in range(num_episodes):

    episodeBuffer = []

    # Reset environment and get first new observation
    sP = env.reset()
    s = torch.FloatTensor(sP).to(device).transpose(0, 2).transpose(1, 2).unsqueeze(dim=0)

    d = False
    rAll = 0
    j = 0

    # Reset the recurrent layer's hidden state
    state = (torch.FloatTensor(np.zeros([1, 1, h_size])).to(device),
             torch.FloatTensor(np.zeros([1, 1, h_size])).to(device))

    # The Q-Network
    while j < max_epLength:

        mainQN.eval()
        j += 1

        # Choose an action by greedily (with e chance of random action) from the Q-network
        if np.random.rand(1) < e or total_steps < pre_train_steps:
            _, _, state1 = mainQN(input=s/255, h=state[0], c=state[1])
            a = np.random.randint(0, 4)
        else:
            _, a, state1 = mainQN(input=s/255, h=state[0], c=state[1])
            a = a.item()

        s1P, r, d = env.step(a)
        s1 = torch.FloatTensor(s1P).to(device).transpose(0, 2).transpose(1, 2).unsqueeze(dim=0)

        total_steps += 1
        episodeBuffer.append(np.reshape(np.array([s.cpu().detach().numpy(), a, r, s1.cpu().detach().numpy(), d]), [1, 5]))

        if total_steps > pre_train_steps:

            if e > endE:
                e -= stepDrop

            if total_steps % update_freq == 0:

                targetQN = updateTargetPytorch(targetQN, mainQN, tau)

                # Get a random batch of experiences
                trainBatch = myBuffer.sample(batch_size, trace_length)  # Get a random batch of experiences.

                # Reset the recurrent layer's hidden state
                state_train = (torch.FloatTensor(np.zeros([1, batch_size, h_size])).to(device),
                                torch.FloatTensor(np.zeros([1, batch_size, h_size])).to(device))

                # Below we perform the Double-DQN update to the target Q-values
                input = torch.FloatTensor(np.vstack(trainBatch[:, 3] / 255.0)).to(device)

                with torch.no_grad():

                    Q1, a1, _ = mainQN(input=input, h=state_train[0], c=state_train[1], trace=True)
                    Q2, a2, _ = targetQN(input=input, h=state_train[0], c=state_train[1], trace=True)

                    end_multipler = -(trainBatch[:, 4] - 1)

                    #doubleQ = Q2[:, a1]
                    doubleQ = Q2[torch.arange(Q2.size(0)).long(), a1]

                    e0 = torch.FloatTensor(trainBatch[:, 2].astype("float")).to(device)
                    e1 = torch.FloatTensor(end_multipler.astype("float")).to(device)
                    targetQ = e0 + (y * doubleQ * e1)

                # Compute loss and train
                mainQN.train()
                optimizer.zero_grad()

                input = torch.FloatTensor(np.vstack(trainBatch[:, 0]/255.0)).to(device)
                Q, a, _ = mainQN(input=input, h=state_train[0], c=state_train[1], trace=True)
                actions = trainBatch[:, 1].astype("int")
                one_hot_actions = np.zeros((len(actions), 4))
                one_hot_actions[np.arange(len(actions)), actions] = 1
                one_hot_actions = torch.FloatTensor(one_hot_actions).to(device)

                temp1 = Q * one_hot_actions
                temp2 = torch.sum(temp1, dim=1)

                # In order to only propagate accurate gradients through the network, we will mask the first
                # half of the losses for each trace as per Lample & Chatlot 2016
                maskA = torch.zeros([batch_size, trace_length // 2]).to(device)
                maskB = torch.ones([batch_size, trace_length // 2]).to(device)
                mask = torch.cat([maskA, maskB], dim=1)
                mask = mask.view(-1)

                # NB: REMOVE THE FOLLOWING LINE
                #mask = torch.ones([32]).to(device)

                td_error = (targetQ.detach() - temp2)**2

                loss = torch.mean(td_error * mask)

                loss.backward()

                optimizer.step()


        rAll += r
        s = s1
        sP = s1P
        state = state1
        if d == True:
            break

    # Add the episode to the experience buffer
    myBuffer.add(episodeBuffer)
    jList.append(j)
    rList.append(rAll)

    if len(rList) % summaryLength == 0 and len(rList) != 0:
        print (i, total_steps, np.mean(rList[-summaryLength:]), e)
        #saveToCenter(i, rList, jList, np.reshape(np.array(episodeBuffer), [len(episodeBuffer), 5]), \
        #             summaryLength, h_size, sess, mainQN, time_per_step)

print("Percent of successful episodes: ", str(sum(rList)/num_episodes) + "%")