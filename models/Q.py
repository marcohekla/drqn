import torch
import torch.nn as nn

class Q(nn.Module):
    def __init__(self, device, h_size=512):
        super(Q, self).__init__()

        self.device = device
        self.h_size = h_size

        # The network receives a frame from the game
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=8, stride=4, bias=False)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2, bias=False)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, bias=False)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=self.h_size, kernel_size=7, stride=1, bias=False)

        # We take the output from the final convolutional layer and send it to a recurrent layer.
        # The input must be reshaped into [batch, trace, units] for rnn processing,
        # and then returned to [batch, units] when sent through the upper levels.
        self.lstm = nn.LSTM(self.h_size, self.h_size)

        self.AW = nn.Parameter(torch.rand(int(self.h_size/2), 4))
        self.VM = nn.Parameter(torch.rand(int(self.h_size/2), 1))


    def forward(self, input, h, c, trace=False, batch_size=4, trace_length=8):

        input = input.to(self.device)
        h = h.to(self.device)
        c = c.to(self.device)

        #print(input.shape)
        out = self.conv1(input)
        #print(out.shape)
        out = self.conv2(out)
        #print(out.shape)
        out = self.conv3(out)
        #print(out.shape)
        out = self.conv4(out)
        #print(out.shape)

        if trace:

            out = out[:, :, 0, 0 ].view(batch_size, trace_length, out.size()[1]).transpose(0, 1)

            out, (h, c) = self.lstm(out, (h, c))

            h1, c1 = h, c
            out = out.transpose(0, 1).contiguous()
            out = out.view(-1, out.size()[2])

        else:

            #out = out[:, :, :, 0].transpose(0, 2).transpose(1, 2)
            out = out[:, :, :, 0].transpose(1, 2)

            out, (h1, c1) = self.lstm(out, (h, c))

            out = out[0, :, :]

        # The output from the recurrent player is then split into separate Value and Advantage streams
        streamA = out[:, :int(out.size()[1]/2)] # trace==False -> 1x256
        streamV = out[:, int(out.size()[1]/2):] # trace==False -> 1x256

        Advantage = streamA @ self.AW # trace==False: 1x4
        Value = streamV @ self.VM # trace==False: 1x1

        # Then combine them together to get our final Q-values
        Qout = Value + (Advantage - torch.mean(Advantage, dim=1).unsqueeze(dim=-1))

        predict = torch.argmax(Qout, dim=1)

        return Qout, predict, (h1, c1)
